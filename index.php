<?php
  if (isset($_POST["submit"])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $human = intval($_POST['human']);
    $from = 'Message From Portfolio';
    $to = 'asingleton6@gmail.com';
    $subject = 'Message from Contact Demo ';

    $body = "From: $name\n E-Mail: $email\n Message:\n $message";

    // Check if name has been entered
    if (!$_POST['name']) {
      $errName = 'Please enter your name';
    }

    // Check if email has been entered and is valid
    if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      $errEmail = 'Please enter a valid email address';
    }

    //Check if message has been entered
    if (!$_POST['message']) {
      $errMessage = 'Please enter your message';
    }

    //Check if simple anti-bot test is correct
    if ($human !== 5) {
      $errHuman = 'Your anti-spam is incorrect';
    }

// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
  if (mail ($to, $subject, $body, $from)) {
    $result='<div class="alert alert-success">Thank You! I will be in touch</div>';
  } else {
    $result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
  }
}
  }
?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <meta name="google" value="notranslate">

    <title>Ashley Singleton's Portfolio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-control" content="public">
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58930887-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments)};
      gtag('js', new Date());

      gtag('config', 'UA-58930887-1');
    </script>
  </head>

  <body>

    <div class="container-fluid">
      <div class="row">
      <!-- Left Content -->
      <div id="left-bg" class="col-lg-6 col-md-12">
        <div id="left-wrapper">
          <a id="logo"><img src="../img/logo.png"/></a>
          <div id="intro-wrapper">
            <h1>Hello & Welcome.</h1>
          </div>
        </div>
      </div>

      <!-- Right Content -->
      <div class="col-lg-6 col-md-12">
        <div id="right-wrapper">
          <!-- About Section -->
          <div id="about" class="row">
            <div class="col-md-12">
              <div class="about-wrapper">
                <button id="click-about-btn" type="button" class="btn active">About Me</button>
                <div id="about-content">
                  <p>
                My name is Ashley, and I’m a web designer and developer with a passion for building beautiful, functional sites. I’ve built custom websites for clients in industries ranging from insurance to tech.  Whether you’re an early stage start up, a mom and pop, or a large and firmly established company, I’d love to build you the site of your dreams! I have a diverse array of language skills, including HTML5, CSS3, Javascript, jQuery, and even PHP.  Let’s chat!
                  </p>
                  <ul class="list-unstyled list-inline list-social-icons">
                    <li class="list-inline-item">
                      <a href="https://linkedin.com/in/ashley-singleton-0a573445" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></a>
                    </li>
                    <li class="list-inline-item">
                      <a href="https://bitbucket.org/asingleton6/" target="_blank"><i class="fa fa-bitbucket-square fa-2x"></i></a>
                    </li>
                    <li class="list-inline-item">
                      <a href="http://codepen.io/asingleton6/" target="_blank"><i class="fa fa-codepen fa-2x"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- Portfolio Section -->
          <div id="portfolio" class="row">
            <div class="col-lg-12">
              <div class="portfolio-wrapper">
                <button id="click-portfolio-btn" type="button" class="btn">Portfolio</button>
                <div id="portfolio-content" class="hide">
                  <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <div class="img-wrapper">
                        <a href="#" data-toggle="modal" data-target="#port1">
                          <img class="thumbnail img-fluid" src="../img/mcclain-thumb.png"/>
                        </a>
                      </div>
                      <!-- Modal -->
                      <div class="modal fade" id="port1" tabindex="-1" role="dialog" aria-labelledby="port1" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <a href="http://www.mcclainstreeexperts.com/" target="_blank">
                              <img class="img-fluid" src="../img/mcclain-full.png" alt="">
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="img-wrapper">
                        <a href="#" data-toggle="modal" data-target="#port2">
                          <img class="thumbnail img-fluid" src="../img/roof-thumb.png"/>
                        </a>
                      </div>
                      <!-- Modal -->
                      <div class="modal fade" id="port2" tabindex="-1" role="dialog" aria-labelledby="port2" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <a href="http://www.conradsroofing.com/" target="_blank">
                              <img class="img-fluid" src="../img/roof-full.png" alt="">
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <div class="img-wrapper">
                        <a href="#" data-toggle="modal" data-target="#port3">
                          <img class="thumbnail img-fluid" src="../img/AGIS-thumb.png"/>
                        </a>
                      </div>
                      <!-- Modal -->
                      <div class="modal fade" id="port3" tabindex="-1" role="dialog" aria-labelledby="port3" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <a href="http://www.25d4ca8555ec42e99d0ff598b3f30e1d.sites.yp.com/" target="_blank">
                              <img class="img-fluid" src="../img/AGIS-full.png" alt="">
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="img-wrapper">
                        <a href="#" data-toggle="modal" data-target="#port4">
                          <img class="thumbnail img-fluid" src="../img/plumb-thumb.png"/>
                        </a>
                      </div>
                      <!-- Modal -->
                      <div class="modal fade" id="port4" tabindex="-1" role="dialog" aria-labelledby="port4" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <a href="http://www.e8de3c2b9cee4a5aa7c666197531185f.sites.yp.com/" target="_blank">
                              <img class="img-fluid" src="../img/plumb-full.png" alt="">
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <div class="img-wrapper">
                        <a href="#" data-toggle="modal" data-target="#port5">
                          <img class="thumbnail img-fluid" src="../img/shore-thumb.png"/>
                        </a>
                      </div>
                      <!-- Modal -->
                      <div class="modal fade" id="port5" tabindex="-1" role="dialog" aria-labelledby="port5" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <a href="http://www.e57f33be9dc740cbb524604a086df337.sites.yp.com/" target="_blank">
                              <img class="img-fluid" src="../img/shore-full.png" alt="">
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="img-wrapper">
                        <a href="#" data-toggle="modal" data-target="#port6">
                          <img class="thumbnail img-fluid" src="../img/rental-thumb.png"/>
                        </a>
                      </div>
                      <!-- Modal -->
                      <div class="modal fade" id="port6" tabindex="-1" role="dialog" aria-labelledby="port6" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <a href="http://www.everjoyrental.com/" target="_blank">
                              <img class="img-fluid" src="../img/rental-full.png" alt="">
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Contact Section -->
          <div id="contact" class="row">
            <div class="col-lg-12">
              <div class="contact-wrapper">
                <button id="click-contact-btn" type="button" class="btn">Contact</button>
                <div id="contact-content" class="hide">
                  <div class="row">
                    <div class="col-lg-12">
                      <form role="form" method="post" action="index.php">
                        <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6">
                            <fieldset class="form-group">
                              <input type="name" class="form-control" name="name" placeholder="Full Name" value="<?php echo htmlspecialchars($_POST['name']); ?>">
                              <?php echo "<p class='text-danger'>$errName</p>";?>
                            </fieldset>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6">
                            <fieldset class="form-group">
                              <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?php echo htmlspecialchars($_POST['email']); ?>">
                              <?php echo "<p class='text-danger'>$errEmail</p>";?>
                            </fieldset>
                          </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                          <div class="col-lg-12 col-md-12">
                            <fieldset class="form-group">
                              <textarea class="form-control" rows="7" name="message" placeholder="Message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
                              <?php echo "<p class='text-danger'>$errMessage</p>";?>
                            </fieldset>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6">
                            <label>Human Verification:</label>
                            <label for="human" class="control-label human">2 + 3 = ?</label>
                            <input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
                            <?php echo "<p class='text-danger'>$errHuman</p>";?>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6">
                            <input id="submit" name="submit" type="submit" value="Send Message" class="btn btn-default"></input>
                          </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                          <div class="col-lg-12">
                            <?php echo $result ?>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>
      </div>
      </div>
    </div>
    <script src="script.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>
